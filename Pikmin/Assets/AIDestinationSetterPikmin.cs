﻿using UnityEngine;
using System.Collections;

namespace Pathfinding
{
    /// <summary>
    /// Sets the destination of an AI to the position of a specified object.
    /// This component should be attached to a GameObject together with a movement script such as AIPath, RichAI or AILerp.
    /// This component will then make the AI move towards the <see cref="target"/> set on this component.
    ///
    /// See: <see cref="Pathfinding.IAstarAI.destination"/>
    ///
    /// [Open online documentation to see images]
    /// </summary>
    [UniqueComponent(tag = "ai.destination")]
    [HelpURL("http://arongranberg.com/astar/docs/class_pathfinding_1_1_a_i_destination_setter.php")]
    public class AIDestinationSetterPikmin : VersionedMonoBehaviour
    {
        /// <summary>The object that the AI should move to</summary>
        public Transform target;
        IAstarAI ai;
        private GameObject Olimar;
        private GameObject targetobject;
        private GameObject controller;

        float r;
        float r2;

        public enum PikminStates
        {
            NONE,
            OLIMARPATH,
            TARGETPATH
        }

        public PikminStates state = PikminStates.NONE;
        Vector3 lastTargetPostition;

        void OnEnable()
        {
            ai = GetComponent<IAstarAI>();
            // Update the destination right before searching for a path as well.
            // This is enough in theory, but this script will also update the destination every
            // frame as the destination is used for debugging and may be used for other things by other
            // scripts as well. So it makes sense that it is up to date every frame.
            if (ai != null) ai.onSearchPath += Update;

            Olimar = GameObject.Find("Olimar");
            targetobject = GameObject.Find("Target");
            controller = GameObject.Find("GameController");

            Olimar.GetComponent<Olimar>().UpdateTargetEvent += updateOlimarPath;

            r = Random.Range(-0.3f, 0.3f);
            r2 = Random.Range(-0.3f, 0.3f);

        }

        void OnDisable()
        {
            if(Olimar != null)
                 Olimar.GetComponent<Olimar>().UpdateTargetEvent -= updateOlimarPath;
            if (ai != null) ai.onSearchPath -= Update;
        }

        /// <summary>Updates the AI's destination every frame</summary>
        void Update()
        {
            if (state != PikminStates.OLIMARPATH && Input.GetKeyDown("z")) //Crida als Pikmin i comencen a seguir a Olimar
            {
                target = Olimar.GetComponent<Transform>();
                state = PikminStates.OLIMARPATH;
                if(this.GetComponent<Pikmin>().getPikminType() == 0)
                {
                    controller.GetComponent<Controller>().listRedPikmin.AddFirst(this.gameObject);
                    controller.GetComponent<Controller>().countFollowingPikmin();
                }
                else if (this.GetComponent<Pikmin>().getPikminType() == 1)
                {
                    controller.GetComponent<Controller>().listBluePikmin.AddFirst(this.gameObject);
                    controller.GetComponent<Controller>().countFollowingPikmin();
                }
                else if (this.GetComponent<Pikmin>().getPikminType() == 2)
                {
                    controller.GetComponent<Controller>().listYellowPikmin.AddFirst(this.gameObject);
                    controller.GetComponent<Controller>().countFollowingPikmin();
                }
                r = Random.Range(-0.3f, 0.3f);
                r2 = Random.Range(-0.3f, 0.3f);
            }

            switch (state)
            {
                case PikminStates.NONE:
                    break;
                case PikminStates.OLIMARPATH:
                    if (target != null && ai != null)
                    {
                        switch (Olimar.GetComponent<Olimar>().getDirection())
                        {
                            case 0:
                                ai.destination = new Vector2(target.position.x + 0.6f + r, target.position.y + 0.6f + r2); //Fa que cada Pikmin es col·loqui en una direcció al voltant de Olimar diferent
                                break;
                            case 1:
                                ai.destination = new Vector2(target.position.x - 0.5f + r, target.position.y - 0.5f + r2);
                                break;
                            case 2:
                                ai.destination = new Vector2(target.position.x + 0.5f + r, target.position.y - 0.5f + r2);
                                break;
                            case 3:
                                ai.destination = new Vector2(target.position.x + 0.6f + r, target.position.y + 0.6f + r2);
                                break;
                        }
                    }
                    break;
                case PikminStates.TARGETPATH:
                    ai.destination = lastTargetPostition;
                    break;
            }
        }

        //Quan es fa doble click a sobre d'un Pikmin, aquest es fica dins la LinkedList de Pikmin que segueixen a Olimar
        private void OnMouseDown()
        {
            if(state == PikminStates.TARGETPATH)
            {
                target = Olimar.GetComponent<Transform>();
                state = PikminStates.OLIMARPATH;
                if (this.GetComponent<Pikmin>().getPikminType() == 0)
                {
                    controller.GetComponent<Controller>().listRedPikmin.AddFirst(this.gameObject);
                    controller.GetComponent<Controller>().countFollowingPikmin();
                }
                else if (this.GetComponent<Pikmin>().getPikminType() == 1)
                {
                    controller.GetComponent<Controller>().listBluePikmin.AddFirst(this.gameObject);
                    controller.GetComponent<Controller>().countFollowingPikmin();
                }
                else if (this.GetComponent<Pikmin>().getPikminType() == 2)
                {
                    controller.GetComponent<Controller>().listYellowPikmin.AddFirst(this.gameObject);
                    controller.GetComponent<Controller>().countFollowingPikmin();
                }
                controller.GetComponent<Controller>().sfxCallPikmin();
            }
            else if(state == PikminStates.NONE)
            {
                target = Olimar.GetComponent<Transform>();
                state = PikminStates.OLIMARPATH;
                if (this.GetComponent<Pikmin>().getPikminType() == 0)
                {
                    controller.GetComponent<Controller>().listRedPikmin.AddFirst(this.gameObject);
                    controller.GetComponent<Controller>().countFollowingPikmin();
                }
                else if (this.GetComponent<Pikmin>().getPikminType() == 1)
                {
                    controller.GetComponent<Controller>().listBluePikmin.AddFirst(this.gameObject);
                    controller.GetComponent<Controller>().countFollowingPikmin();
                }
                else if (this.GetComponent<Pikmin>().getPikminType() == 2)
                {
                    controller.GetComponent<Controller>().listYellowPikmin.AddFirst(this.gameObject);
                    controller.GetComponent<Controller>().countFollowingPikmin();
                }
                controller.GetComponent<Controller>().sfxCallPikmin();
            }
            
        }

        //Funció que updateja el path cap a Olimar, és a dir, els Pikmin van seguint a Olimar en tot moment
        private void updateOlimarPath()
        {
            ai.SearchPath();
           
        }

        //Deixa el Pikmin en l'últim lloc on s'ha fet doble click
        public void setLastTargetPosition(Vector3 p)
        {
            this.lastTargetPostition = p; 
        }

        public Vector3 getAIDestination()
        {
            return this.ai.destination;
        }
    }
}
