﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PikminUI : MonoBehaviour
{
    private int numNewPikmin;
    public GameObject text;
    public GameObject ship;
    public GameObject ship2;
    public GameObject ship3;
    public GameObject controller;
    public GameObject pikmin;

    //Els Pikmin que vol crear nous el jugador
    private int maxNewPikmin;

    public int colorType;

    public delegate void sumPikminController(int n);
    public event sumPikminController sumPikminControllerEvent;

    public GameObject targetMover;

    // Start is called before the first frame update
    void Start()
    {

    }

    void OnEnable()
    {
        numNewPikmin = 0;
        targetMover.GetComponent<Pathfinding.TargetMover>().activeMouse = false;
    }

    // Update is called once per frame
    void Update()
    {
        //PRINT DELS PIKMIN QUE ES PODEN CREAR
        if (colorType == 0)
        {
            if(numNewPikmin < 10 && controller.GetComponent<Controller>().redDrop < 10)
            {
                text.gameObject.GetComponent<TMPro.TextMeshPro>().text = "0"+numNewPikmin + "/0" + controller.GetComponent<Controller>().redDrop;
            }
            else if (numNewPikmin < 10 && controller.GetComponent<Controller>().redDrop >= 10)
            {
                text.gameObject.GetComponent<TMPro.TextMeshPro>().text = "0" + numNewPikmin + "/" + controller.GetComponent<Controller>().redDrop;
            }
            else if (numNewPikmin >= 10 && controller.GetComponent<Controller>().redDrop < 10)
            {
                text.gameObject.GetComponent<TMPro.TextMeshPro>().text = + numNewPikmin + "/0" + controller.GetComponent<Controller>().redDrop;
            }
            else
            {
                text.gameObject.GetComponent<TMPro.TextMeshPro>().text = numNewPikmin + "/" + controller.GetComponent<Controller>().redDrop;
            }
        }
        else if (colorType == 1)
        {

            if (numNewPikmin < 10 && controller.GetComponent<Controller>().blueDrop < 10)
            {
                text.gameObject.GetComponent<TMPro.TextMeshPro>().text = "0" + numNewPikmin + "/0" + controller.GetComponent<Controller>().blueDrop;
            }
            else if (numNewPikmin < 10 && controller.GetComponent<Controller>().blueDrop >= 10)
            {
                text.gameObject.GetComponent<TMPro.TextMeshPro>().text = "0" + numNewPikmin + "/" + controller.GetComponent<Controller>().blueDrop;
            }
            else if (numNewPikmin >= 10 && controller.GetComponent<Controller>().blueDrop < 10)
            {
                text.gameObject.GetComponent<TMPro.TextMeshPro>().text = +numNewPikmin + "/0" + controller.GetComponent<Controller>().blueDrop;
            }
            else
            {
                text.gameObject.GetComponent<TMPro.TextMeshPro>().text = numNewPikmin + "/" + controller.GetComponent<Controller>().blueDrop;
            }
        }
        else if (colorType == 2)
        {
            if (numNewPikmin < 10 && controller.GetComponent<Controller>().yellowDrop < 10)
            {
                text.gameObject.GetComponent<TMPro.TextMeshPro>().text = "0" + numNewPikmin + "/0" + controller.GetComponent<Controller>().yellowDrop;
            }
            else if (numNewPikmin < 10 && controller.GetComponent<Controller>().yellowDrop >= 10)
            {
                text.gameObject.GetComponent<TMPro.TextMeshPro>().text = "0" + numNewPikmin + "/" + controller.GetComponent<Controller>().yellowDrop;
            }
            else if (numNewPikmin >= 10 && controller.GetComponent<Controller>().yellowDrop < 10)
            {
                text.gameObject.GetComponent<TMPro.TextMeshPro>().text = +numNewPikmin + "/0" + controller.GetComponent<Controller>().yellowDrop;
            }
            else
            {
                text.gameObject.GetComponent<TMPro.TextMeshPro>().text = numNewPikmin + "/" + controller.GetComponent<Controller>().yellowDrop;
            }
        }
    }

    //Es creen nou Pikmin
    void newPikmin()
    {
        pikmin.gameObject.GetComponent<Transform>().position = new Vector2(ship.GetComponent<Transform>().position.x, ship.GetComponent<Transform>().position.y);
        for (int i = 0; i < numNewPikmin; i++)
        {
            GameObject newpikmin = Instantiate(pikmin);
            newpikmin.GetComponent<Pikmin>().setPikminType(colorType);
            if (colorType == 1)
            {
                controller.gameObject.GetComponent<Controller>().blueDrop--;
            }
            else if (colorType == 0)
            {
                controller.gameObject.GetComponent<Controller>().redDrop--;
            }
            else if (colorType == 2)
            {
                controller.gameObject.GetComponent<Controller>().yellowDrop--;
            }
            pikmin.GetComponent<Pikmin>().updatePikminUInumEvent += incPikminUi;
        }

    }
    
    public int getNumPikmin()
    {
        return this.numNewPikmin;
    }

    public void setNumPikmin(int n)
    {
        numNewPikmin = n;
    }

    public void sumPikmin()
    {
        if (colorType == 1)
        {
            if (numNewPikmin < controller.GetComponent<Controller>().blueDrop)
            {
                numNewPikmin++;
            }
        }
        else if (colorType == 0)
        {
            if (numNewPikmin < controller.GetComponent<Controller>().redDrop)
            {
                numNewPikmin++;
                
            }
        }
        else if (colorType == 2)
        {
            if (numNewPikmin < controller.GetComponent<Controller>().yellowDrop)
            {
                numNewPikmin++;
            }
        }
    }

    public void minPikmin()
    {
        if (numNewPikmin > 0)
        {
            numNewPikmin--;
        }

    }

    //Accepta quants Pikmin crear i crida a la funció de creació Pikmin
    public void acceptPikmin()
    {
        this.gameObject.SetActive(false);
        ship.gameObject.GetComponent<BoxCollider2D>().enabled = true;
        ship2.gameObject.GetComponent<BoxCollider2D>().enabled = true;
        ship3.gameObject.GetComponent<BoxCollider2D>().enabled = true;
        newPikmin();
        if(sumPikminControllerEvent != null)
        {
            sumPikminControllerEvent.Invoke(numNewPikmin);
        }
        targetMover.GetComponent<Pathfinding.TargetMover>().activeMouse = true;
    }

    void incPikminUi()
    {
        controller.GetComponent<Controller>().nPikmins++;
    }

}
