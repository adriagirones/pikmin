﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class hpUI : MonoBehaviour
{
    public Sprite[] hpOlimarPhoto;
    public GameObject Olimar;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        //Updateja la imatge de vida de l'Olimar
        if (Olimar.GetComponent<Olimar>().hp == 100)
        {
            this.GetComponent<SpriteRenderer>().sprite = hpOlimarPhoto[0];
        }
        else if (Olimar.GetComponent<Olimar>().hp <= 99 && Olimar.GetComponent<Olimar>().hp > 90)
        {
            this.GetComponent<SpriteRenderer>().sprite = hpOlimarPhoto[1]; 
        }
        else if (Olimar.GetComponent<Olimar>().hp <= 89 && Olimar.GetComponent<Olimar>().hp >= 80)
        {
            this.GetComponent<SpriteRenderer>().sprite = hpOlimarPhoto[2];
        }
        else if (Olimar.GetComponent<Olimar>().hp <= 79 && Olimar.GetComponent<Olimar>().hp >= 70)
        {
            this.GetComponent<SpriteRenderer>().sprite = hpOlimarPhoto[3];
        }
        else if (Olimar.GetComponent<Olimar>().hp <= 69 && Olimar.GetComponent<Olimar>().hp >= 60)
        {
            this.GetComponent<SpriteRenderer>().sprite = hpOlimarPhoto[4];
        }
        else if (Olimar.GetComponent<Olimar>().hp <= 59 && Olimar.GetComponent<Olimar>().hp >= 50)
        {
            this.GetComponent<SpriteRenderer>().sprite = hpOlimarPhoto[5];
        }
        else if (Olimar.GetComponent<Olimar>().hp <= 49 && Olimar.GetComponent<Olimar>().hp >= 40)
        {
            this.GetComponent<SpriteRenderer>().sprite = hpOlimarPhoto[6];
        }
        else if (Olimar.GetComponent<Olimar>().hp <= 39 && Olimar.GetComponent<Olimar>().hp >= 30)
        {
            this.GetComponent<SpriteRenderer>().sprite = hpOlimarPhoto[7];
        }
        else if (Olimar.GetComponent<Olimar>().hp <= 29 && Olimar.GetComponent<Olimar>().hp >= 20)
        {
            this.GetComponent<SpriteRenderer>().sprite = hpOlimarPhoto[8];
        }
        else if (Olimar.GetComponent<Olimar>().hp <= 19 && Olimar.GetComponent<Olimar>().hp >= 10)
        {
            this.GetComponent<SpriteRenderer>().sprite = hpOlimarPhoto[9];
        }
        else if (Olimar.GetComponent<Olimar>().hp <= 9 && Olimar.GetComponent<Olimar>().hp >= 5)
        {
            this.GetComponent<SpriteRenderer>().sprite = hpOlimarPhoto[10];
        }
        else if (Olimar.GetComponent<Olimar>().hp < 5)
        {
            this.GetComponent<SpriteRenderer>().sprite = hpOlimarPhoto[11];
        }
    }
}
