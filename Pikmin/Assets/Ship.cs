﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ship : MonoBehaviour
{
    public GameObject pikminUI;
    public GameObject ship;
    public GameObject ship2;

    public Sprite[] frames_animation;
    Animation2D animationShip;

    // Start is called before the first frame update
    void Start()
    {
        animationShip = new Animation2D(frames_animation);
        animationShip.speed = 0.2f;
    }

    // Update is called once per frame
    void Update()
    {
        this.GetComponent<SpriteRenderer>().sprite = animationShip.GetCurrentFrame();
    }

    //Quan es fa un click a sobre d'una nau es desactiven els colliders de les altres ja que no volem que surtin totes les pantalles alhora
    private void OnMouseDown()
    {       
            pikminUI.gameObject.SetActive(true);
            this.gameObject.GetComponent<BoxCollider2D>().enabled = false;
            ship.gameObject.GetComponent<BoxCollider2D>().enabled = false;
            ship2.gameObject.GetComponent<BoxCollider2D>().enabled = false;
    }
}
