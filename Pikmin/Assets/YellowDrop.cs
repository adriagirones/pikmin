﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class YellowDrop : MonoBehaviour
{
    public GameObject controller;
    bool flag;

    // Start is called before the first frame update
    void Start()
    {
        controller = GameObject.Find("GameController");
        this.GetComponent<BoxCollider2D>().enabled = false;
        Invoke("ActiveCollider", 0.5f);
        flag = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (flag)
        {
            Destroy(this.gameObject);
            controller.GetComponent<Controller>().yellowDrop++;
        }
    }

    private void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.CompareTag("Player") || collider.CompareTag("Pikmin"))
        {
            Invoke("deleteDrop", 0.1f);
        }
    }

    private void deleteDrop()
    {
        flag = true;
        this.GetComponent<BoxCollider2D>().enabled = false;
    }

    //Activa el collider al cap d'una estona així es mostra visualment durant uns segons
    private void ActiveCollider()
    {
        this.GetComponent<BoxCollider2D>().enabled = true;
    }
}
