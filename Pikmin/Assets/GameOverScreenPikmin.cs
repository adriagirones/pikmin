﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameOverScreenPikmin : MonoBehaviour
{
    //easteregg
    public int vel;
    bool move;
    // Start is called before the first frame update
    void Start()
    {
        Invoke("activeMove", 6f);
    }

    // Update is called once per frame
    void Update()
    {
        if (move)
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(-vel, +vel);
            this.GetComponent<Transform>().Rotate((Vector3.forward));
        }
       
    }

    void activeMove()
    {
        move = true;
    }
}
