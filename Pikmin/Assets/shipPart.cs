﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class shipPart : MonoBehaviour
{
    //Parts de la nau que s'han de recollir
    public enum ShipParts
    {
        SHIP_PART1,
        SHIP_PART2,
        SHIP_PART3,
    }

    public ShipParts typeShip;
    public GameObject activateUIpart;
    GameObject controller;

    // Start is called before the first frame update
    void Start()
    {
        controller = GameObject.Find("GameController");   
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Pikmin") || collision.gameObject.CompareTag("Player"))
        {
            activateUIpart.GetComponent<UnityEngine.UI.Image>().color = Color.white;
            switch (typeShip)
            {
                case ShipParts.SHIP_PART1:
                    controller.GetComponent<Controller>().shipPart1 = true;
                    break;
                case ShipParts.SHIP_PART2:
                    controller.GetComponent<Controller>().shipPart2 = true;
                    break;
                case ShipParts.SHIP_PART3:
                    controller.GetComponent<Controller>().shipPart3 = true;
                    break;
            }

            Destroy(this.gameObject);
        }
    }

}
