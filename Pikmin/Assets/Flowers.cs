﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flowers : MonoBehaviour
{
    public GameObject redDrop;
    public GameObject blueDrop;
    public GameObject yellowDrop;

    

    public enum FlowerType { RED, BLUE, YELLOW};
    public FlowerType flowerColor;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    //Si colisiona amb un Pikmin fa un drop segons el color de la flor.
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Pikmin"))
        {
            if (flowerColor == FlowerType.RED)
            {
                redDrop.gameObject.GetComponent<Transform>().position = new Vector2(this.GetComponent<Transform>().position.x, this.GetComponent<Transform>().position.y - 0.5f);
                Instantiate(redDrop);
                Destroy(this.gameObject);
            }
            else if(flowerColor == FlowerType.BLUE)
            {
                blueDrop.gameObject.GetComponent<Transform>().position = new Vector2(this.GetComponent<Transform>().position.x, this.GetComponent<Transform>().position.y - 0.5f);
                Instantiate(blueDrop);
                Destroy(this.gameObject);
            }
            else if(flowerColor == FlowerType.YELLOW)
            {
                yellowDrop.gameObject.GetComponent<Transform>().position = new Vector2(this.GetComponent<Transform>().position.x, this.GetComponent<Transform>().position.y - 0.5f);
                Instantiate(yellowDrop);
                Destroy(this.gameObject);
            }
        }
    }
}
