﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExtraPart : MonoBehaviour
{
    //COLECCIONABLES EXTRA
    public Sprite extra1;
    public Sprite extra2;
    public Sprite extra3;
    public Sprite extra4;
    public Sprite extra5;

    int part;

    // Start is called before the first frame update
    void Start()
    {
        Invoke("ActiveCollider", 0.7f);
        this.GetComponent<BoxCollider2D>().enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Pikmin") || collision.gameObject.CompareTag("Player"))
        {
            switch (part)
            {
                case 1:
                    Dades.extra1 = true;
                    break;
                case 2:
                    Dades.extra2 = true;
                    break;
                case 3:
                    Dades.extra3 = true;
                    break;
                case 4:
                    Dades.extra4 = true;
                    break;
                case 5:
                    Dades.extra5 = true;
                    break;
                default:
                    break;
            }

            Destroy(this.gameObject);
        }
    }

    public void setPart(int n)
    {
        part = n;
        switch (part)
        {
            case 1:
                this.GetComponent<SpriteRenderer>().sprite = extra1;
                break;
            case 2:
                this.GetComponent<SpriteRenderer>().sprite = extra2;
                break;
            case 3:
                this.GetComponent<SpriteRenderer>().sprite = extra3;
                break;
            case 4:
                this.GetComponent<SpriteRenderer>().sprite = extra4;
                break;
            case 5:
                this.GetComponent<SpriteRenderer>().sprite = extra5;
                break;
            default:
                break;
        }
        
    }

    private void ActiveCollider()
    {
        this.GetComponent<BoxCollider2D>().enabled = true;
    }

}
