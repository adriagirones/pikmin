﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Timer : MonoBehaviour
{
    //Objecte que va calcula el temps que té el jugador per acabar el nivell
    public GameObject dayEnds;
    public int duration;
    public int timeRemaining;
    public AudioSource ching;

    public void Start()
    {
        timeRemaining = duration;
        InvokeRepeating("_tick", 1f, 1f);
        dayEnds.SetActive(false);
    }
    
    void Update()
    {
        this.GetComponent<TMPro.TextMeshProUGUI>().SetText("Time Left: " + timeRemaining);
        if (timeRemaining <= 0)
        {
            SceneManager.LoadScene("GameOver");
        }
        dayEndsin45();

    }
    private void _tick()
    {
        timeRemaining--;
    }

    //Quan queden 45 segons, avisa que està a punt d'acabar el dia
    private void dayEndsin45()
    {
        if (timeRemaining == 45)
        {
            dayEnds.SetActive(true);
            ching.Play();
        }
        if (timeRemaining == 40)
        {
            dayEnds.SetActive(false);
        }
    }
    
}
