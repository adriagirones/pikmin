﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class loadScene : MonoBehaviour
{
    //escena del principi
    public TextMeshProUGUI playerName;
    public AudioSource bgmintro;

    // Start is called before the first frame update
    void Start()
    {
        playerName.gameObject.GetComponent<TextMeshProUGUI>().text = "";
        //reseteja les dades
        Dades.playerName = "";
        Dades.playTime = 0;
        Dades.extra1 = false;
        Dades.extra2 = false;
        Dades.extra3 = false;
        Dades.extra4 = false;
        Dades.extra5 = false;
        bgmintro.Play();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void forestOfHope()
    {
        if (!string.IsNullOrEmpty(playerName.gameObject.GetComponent<TextMeshProUGUI>().text))
        {
            Dades.playerName = playerName.gameObject.GetComponent<TextMeshProUGUI>().text;
        }
        else
        {
            //Posa per defecte el nom Olimiar si no s'escriu cap nom
            Dades.playerName = "Olimar";
        }
            
        SceneManager.LoadScene("Mapa1");
    }


}
