﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ControllerWin : MonoBehaviour
{
    public GameObject part1;
    public GameObject part2;
    public GameObject part3;
    public GameObject part4;
    public GameObject part5;
    public GameObject timetext;
    public GameObject nametext;
    public AudioSource bgm;

    // Start is called before the first frame update
    void Start()
    {
        bgm.Play();
        //Mostra els coleccionables que el jugador ha agafat
        if (Dades.extra1)
            part1.GetComponent<SpriteRenderer>().color = Color.white;
        if (Dades.extra2)
            part2.GetComponent<SpriteRenderer>().color = Color.white;
        if (Dades.extra3)
            part3.GetComponent<SpriteRenderer>().color = Color.white;
        if (Dades.extra4)
            part4.GetComponent<SpriteRenderer>().color = Color.white;
        if (Dades.extra5)
            part5.GetComponent<SpriteRenderer>().color = Color.white;

        //Mostra el nom que el jugador ha introduit.
        nametext.GetComponent<TMPro.TextMeshProUGUI>().text = Dades.playerName;

        //Calcula el temps que ha durat la partida.
        int sec = Dades.playTime;
        int min = sec / 60;
        sec = sec % 60;
        if(sec < 10 && min < 10)
            timetext.GetComponent<TMPro.TextMeshProUGUI>().text = "0"+min+":0"+sec;
        else if (sec < 10)
            timetext.GetComponent<TMPro.TextMeshProUGUI>().text = min + ":0" + sec;
        else if (min < 10)
            timetext.GetComponent<TMPro.TextMeshProUGUI>().text = "0"+min + ":"+ sec;
        else
            timetext.GetComponent<TMPro.TextMeshProUGUI>().text = min + ":" + sec;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    //Carrega la Start scene.
    public void toMainMenu()
    {
        SceneManager.LoadScene("Intro");
    }
}
