﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    //Stats dels enemics
    public float velocity;
    public int hpMax;
    int currentHp;
    private bool flagMove;
    GameObject controller;

    //Tipus d'enemics amb les seves direccions de moviment.
    public enum TypesOfEnemy { BULBMIN_HORIZONTAL, BULBMIN_VERTICAL, PUFFSTOOL_HORIZONTAL, WOLLYWOG, AMPHITUBER_HORIZONTAL, AMPHITUBER_VERTICAL, FIERYBLOWLET_HORIZONTAL };
    public TypesOfEnemy enemyType;

    //Direccions de moviment posibles.
    public enum direction { UP, DOWN, RIGHT, LEFT, NONE };
    public direction currentDirection;

    //Drops dels enemics deixen al ser derrotats.
    public bool randomDrop;
    public enum DropColors { NONE, RED, BLUE, YELLOW, EXTRA1, EXTRA2, EXTRA3, EXTRA4, EXTRA5 };
    public DropColors dropType;

    public GameObject redDrop;
    public GameObject blueDrop;
    public GameObject yellowDrop;
    public GameObject extraDrop;

    public bool respawnEnemy;
    public float respawnTime;
    public float attackingTime;
    public float killingTime;

    public Sprite[] frames_animationUp;
    public Sprite[] frames_animationDown;
    public Sprite[] frames_animationLeft;
    public Sprite[] frames_animationRight;
    Animation2D currentAnimation;
    Animation2D animationUp;
    Animation2D animationDown;
    Animation2D animationLeft;
    Animation2D animationRight;

    public Sprite[] hpImage;
    public GameObject hpImageObject;

    Vector2 initialPosition;
    Vector2 initialPositionHP;
    direction initialDirection;

    //Els estats d'atac funciones de la següent manera
    //NONE -> L'enemic no ha col·lisionat encara amb cap Pikmin
    //ATTACKING -> Està esperant a que s'invoqui la funció d'atacar, quan s'invoqui aquesta funció l'enemic passarà a l'estat KILLING
    //KILLING -> Durant un període de temps, si l'enemic col·lisiona amb un Pikmin, el matarà, hi un cop el mati passarà a l'estat WAITING_TO_ATTACK.
    //Si no col·lisiona amb cap Pikmin també passarà a l'estat WAITING_TO_ATTACK a l'estona
    //WAITING_TO_ATTACK -> Fa el Invoke de la funció d'atacar i passa a l'estat ATTACKING
    enum States { NONE, ATTACKING, WAITING_TO_ATTACK, KILLING };
    States actualState;
    bool killingPikmin;

    // Start is called before the first frame update
    void Start()
    {
        controller = GameObject.Find("GameController");
        flagMove = true;

        animationUp = new Animation2D(frames_animationUp);
        animationUp.speed = 0.3f;

        animationDown = new Animation2D(frames_animationDown);
        animationDown.speed = 0.3f;

        animationLeft = new Animation2D(frames_animationLeft);
        animationLeft.speed = 0.3f;

        animationRight = new Animation2D(frames_animationRight);
        animationRight.speed = 0.3f;

        currentAnimation = animationDown;

        hpImageObject.gameObject.GetComponent<SpriteRenderer>().enabled = false;

       
        switch (enemyType)
        {
            case TypesOfEnemy.BULBMIN_HORIZONTAL:
                InvokeRepeating("Horizontal", 2f, 3f);
                break;
            case TypesOfEnemy.BULBMIN_VERTICAL:
                InvokeRepeating("Vertical", 2f, 3f);
                break;
            case TypesOfEnemy.PUFFSTOOL_HORIZONTAL:
                InvokeRepeating("Horizontal", 0f, 8f);
                break;
            case TypesOfEnemy.WOLLYWOG:
                currentAnimation = animationDown;
                break;
            case TypesOfEnemy.AMPHITUBER_HORIZONTAL:
                InvokeRepeating("Horizontal", 0f, 3f);
                break;
            case TypesOfEnemy.AMPHITUBER_VERTICAL:
                InvokeRepeating("Vertical", 0f, 3f);
                break;
            case TypesOfEnemy.FIERYBLOWLET_HORIZONTAL:
                InvokeRepeating("Horizontal", 0f, 3f);
                break;
            default:
                break;
        }

        //Es guarda la posició incial per si es farà un respawn
        initialPosition = this.GetComponent<Transform>().position;
        initialPositionHP = hpImageObject.GetComponent<Transform>().position;
        initialDirection = this.currentDirection;

        currentHp = hpMax;

        actualState = States.NONE;
        
    }

    // Update is called once per frame
    void Update()
    {
        if (flagMove)
        {
            switch (enemyType)
            {
                case TypesOfEnemy.BULBMIN_HORIZONTAL:
                    if (currentDirection == direction.LEFT)
                    {
                        currentAnimation = animationLeft;
                        this.GetComponent<Rigidbody2D>().velocity = new Vector2(-velocity, velocity);
                    }
                    else if (currentDirection == direction.RIGHT)
                    {
                        currentAnimation = animationRight;
                        this.GetComponent<Rigidbody2D>().velocity = new Vector2(velocity, -velocity);
                    }
                    break;
                case TypesOfEnemy.BULBMIN_VERTICAL:
                    if (currentDirection == direction.DOWN)
                    {
                        currentAnimation = animationDown;
                        this.GetComponent<Rigidbody2D>().velocity = new Vector2(-velocity, -velocity);
                    }
                    else if (currentDirection == direction.UP)
                    {
                        currentAnimation = animationUp;
                        this.GetComponent<Rigidbody2D>().velocity = new Vector2(velocity, velocity);
                    }
                    break;
                case TypesOfEnemy.PUFFSTOOL_HORIZONTAL:
                    if (currentDirection == direction.LEFT)
                    {
                        currentAnimation = animationLeft;
                        this.GetComponent<Rigidbody2D>().velocity = new Vector2(-velocity, velocity);
                    }
                    else if (currentDirection == direction.RIGHT)
                    {
                        currentAnimation = animationRight;
                        this.GetComponent<Rigidbody2D>().velocity = new Vector2(velocity, -velocity);
                    }
                    break;
                case TypesOfEnemy.AMPHITUBER_HORIZONTAL:
                    if (currentDirection == direction.LEFT)
                    {
                        currentAnimation = animationLeft;
                        this.GetComponent<Rigidbody2D>().velocity = new Vector2(-velocity, velocity);
                    }
                    else if (currentDirection == direction.RIGHT)
                    {
                        currentAnimation = animationRight;
                        this.GetComponent<Rigidbody2D>().velocity = new Vector2(velocity, -velocity);
                    }
                    break;
                case TypesOfEnemy.AMPHITUBER_VERTICAL:
                    if (currentDirection == direction.DOWN)
                    {
                        currentAnimation = animationDown;
                        this.GetComponent<Rigidbody2D>().velocity = new Vector2(-velocity, -velocity);
                    }
                    else if (currentDirection == direction.UP)
                    {
                        currentAnimation = animationUp;
                        this.GetComponent<Rigidbody2D>().velocity = new Vector2(velocity, velocity);
                    }
                    break;
                case TypesOfEnemy.FIERYBLOWLET_HORIZONTAL:
                    if (currentDirection == direction.LEFT)
                    {
                        currentAnimation = animationLeft;
                        this.GetComponent<Rigidbody2D>().velocity = new Vector2(-velocity, velocity);
                    }
                    else if (currentDirection == direction.RIGHT)
                    {
                        currentAnimation = animationRight;
                        this.GetComponent<Rigidbody2D>().velocity = new Vector2(velocity, -velocity);
                    }
                    break;
                default:
                    break;
            }

        }
        else
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
        }

        liveUpdate();

        if (currentHp <= 0)
        {
            EnemyDrop();
            if (respawnEnemy)
            {
                this.gameObject.GetComponent<SpriteRenderer>().enabled = false;
                this.gameObject.GetComponent<BoxCollider2D>().enabled = false;
                hpImageObject.gameObject.GetComponent<SpriteRenderer>().enabled = false;
                this.currentHp = this.hpMax;
                Invoke("Respawn", respawnTime);
            }
            else
            {
                Destroy(this.gameObject);
            }
        }
        if (actualState == States.WAITING_TO_ATTACK)
        {
            actualState = States.ATTACKING;
            Invoke("Attack", attackingTime);
        }
        else if (actualState == States.KILLING)
        {
            Invoke("waitToAttack", killingTime);
        }

        this.GetComponent<SpriteRenderer>().sprite = currentAnimation.GetCurrentFrame();
    }

    //Funcio per calcular el drop de l'enemic entre els tres tipus posibles o si es un drop extra.
    private void EnemyDrop()
    {
        if (randomDrop)
        {
            int n = Random.Range(0, 3);

            if (n == 0)
            {
                redDrop.gameObject.GetComponent<Transform>().position = new Vector2(this.GetComponent<Transform>().position.x, this.GetComponent<Transform>().position.y);
                Instantiate(redDrop);
            }
            else if (n == 1)
            {
                blueDrop.gameObject.GetComponent<Transform>().position = new Vector2(this.GetComponent<Transform>().position.x, this.GetComponent<Transform>().position.y);
                Instantiate(blueDrop);
            }
            else if (n == 2)
            {
                yellowDrop.gameObject.GetComponent<Transform>().position = new Vector2(this.GetComponent<Transform>().position.x, this.GetComponent<Transform>().position.y);
                Instantiate(yellowDrop);
            }
        }
        else
        {
            if (dropType == DropColors.RED)
            {
                redDrop.gameObject.GetComponent<Transform>().position = new Vector2(this.GetComponent<Transform>().position.x, this.GetComponent<Transform>().position.y);
                Instantiate(redDrop);
            }
            else if (dropType == DropColors.BLUE)
            {
                blueDrop.gameObject.GetComponent<Transform>().position = new Vector2(this.GetComponent<Transform>().position.x, this.GetComponent<Transform>().position.y);
                Instantiate(blueDrop);
            }
            else if (dropType == DropColors.YELLOW)
            {
                yellowDrop.gameObject.GetComponent<Transform>().position = new Vector2(this.GetComponent<Transform>().position.x, this.GetComponent<Transform>().position.y);
                Instantiate(yellowDrop);
            }
            else if (dropType == DropColors.EXTRA1)
            {
                extraDrop.gameObject.GetComponent<Transform>().position = new Vector2(this.GetComponent<Transform>().position.x, this.GetComponent<Transform>().position.y);
                GameObject reference = Instantiate(extraDrop);
                reference.GetComponent<ExtraPart>().setPart(1);
            }
            else if (dropType == DropColors.EXTRA2)
            {
                extraDrop.gameObject.GetComponent<Transform>().position = new Vector2(this.GetComponent<Transform>().position.x, this.GetComponent<Transform>().position.y);
                GameObject reference = Instantiate(extraDrop);
                reference.GetComponent<ExtraPart>().setPart(2);
            }
            else if (dropType == DropColors.EXTRA3)
            {
                extraDrop.gameObject.GetComponent<Transform>().position = new Vector2(this.GetComponent<Transform>().position.x, this.GetComponent<Transform>().position.y);
                GameObject reference = Instantiate(extraDrop);
                reference.GetComponent<ExtraPart>().setPart(3);
            }
            else if (dropType == DropColors.EXTRA4)
            {
                extraDrop.gameObject.GetComponent<Transform>().position = new Vector2(this.GetComponent<Transform>().position.x, this.GetComponent<Transform>().position.y);
                GameObject reference = Instantiate(extraDrop);
                reference.GetComponent<ExtraPart>().setPart(4);
            }
            else if (dropType == DropColors.EXTRA5)
            {
                extraDrop.gameObject.GetComponent<Transform>().position = new Vector2(this.GetComponent<Transform>().position.x, this.GetComponent<Transform>().position.y);
                GameObject reference = Instantiate(extraDrop);
                reference.GetComponent<ExtraPart>().setPart(5);
            }

        }
        
    }

    //Moviment vertical
    public void Vertical()
    {
        if (flagMove)
        {
            if (currentDirection == direction.DOWN)
            {
                currentDirection = direction.UP;
            }
            else if (currentDirection == direction.UP)
            {
                currentDirection = direction.DOWN;
            }
        }
    }

    //Moviment Horizontal
    public void Horizontal()
    {
        if (flagMove)
        {
            if (currentDirection == direction.LEFT)
            {
                currentDirection = direction.RIGHT;
            }
            else if (currentDirection == direction.RIGHT)
            {
                currentDirection = direction.LEFT;
            }
        }
    }

    //Mort i atac del enemic.
    private void OnCollisionStay2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Pikmin"))
        {
            this.currentHp--;
        }
        if (killingPikmin)
        {
            if (collision.gameObject.CompareTag("Pikmin"))
            {
                collision.gameObject.GetComponent<Pikmin>().Death();
                actualState = States.WAITING_TO_ATTACK;
                killingPikmin = false;
            }     
        }
    }

    //Canvia l'estat de l'enemic a ATTACKING.
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Pikmin"))
        {
            flagMove = false;
            if(actualState == States.NONE) {
                actualState = States.ATTACKING;
                Invoke("Attack", attackingTime);
            }
            
        }
    }

    void Attack()
    {
        actualState = States.KILLING;
        killingPikmin = true;
    }

    void waitToAttack()
    {
        if (killingPikmin)
        {
            actualState = States.WAITING_TO_ATTACK;
            killingPikmin = false;
        }

    }

    //Updateja el indicador de vida que apareix a sobre dels enemics
    private void liveUpdate()
    {
        int hp100 = 0;
        if(currentHp > 0)
        {
           hp100  = (this.currentHp * 100) / this.hpMax;
        }
        if (hp100 != 100)
        {
            hpImageObject.GetComponent<SpriteRenderer>().enabled = true;
        }

        if (hp100 == 100)
        {
            this.hpImageObject.GetComponent<SpriteRenderer>().sprite = hpImage[0];
        }
        else if (hp100 <= 100 && hp100 > 90)
        {
            this.hpImageObject.GetComponent<SpriteRenderer>().sprite = hpImage[1];
        }
        else if (hp100 <= 89 && hp100 >= 80)
        {
            this.hpImageObject.GetComponent<SpriteRenderer>().sprite = hpImage[2];
        }
        else if (hp100 <= 79 && hp100 >= 70)
        {
            this.hpImageObject.GetComponent<SpriteRenderer>().sprite = hpImage[3];
        }
        else if (hp100 <= 69 && hp100 >= 60)
        {
            this.hpImageObject.GetComponent<SpriteRenderer>().sprite = hpImage[4];
        }
        else if (hp100 <= 59 && hp100 >= 50)
        {
            this.hpImageObject.GetComponent<SpriteRenderer>().sprite = hpImage[5];
        }
        else if (hp100 <= 49 && hp100 >= 40)
        {
            this.hpImageObject.GetComponent<SpriteRenderer>().sprite = hpImage[6];
        }
        else if (hp100 <= 39 && hp100 >= 30)
        {
            this.hpImageObject.GetComponent<SpriteRenderer>().sprite = hpImage[7];
        }
        else if (hp100 <= 29 && hp100 >= 20)
        {
            this.hpImageObject.GetComponent<SpriteRenderer>().sprite = hpImage[8];
        }
        else if (hp100 <= 19 && hp100 >= 10)
        {
            this.hpImageObject.GetComponent<SpriteRenderer>().sprite = hpImage[9];
        }
        else if (hp100 <= 9 && hp100 >= 5)
        {
            this.hpImageObject.GetComponent<SpriteRenderer>().sprite = hpImage[10];
        }
        else if (hp100 < 5 && hp100 >= 0)
        {
            this.hpImageObject.GetComponent<SpriteRenderer>().sprite = hpImage[11];
        }
    }

    //Respawn dels enemics si tenen.
    private void Respawn()
    {
        this.gameObject.GetComponent<SpriteRenderer>().enabled = true;
        this.gameObject.GetComponent<BoxCollider2D>().enabled = true;
        this.GetComponent<Transform>().position = initialPosition;
        hpImageObject.GetComponent<Transform>().position = initialPositionHP;
        this.currentDirection = initialDirection;
        flagMove = true;
    }



}
