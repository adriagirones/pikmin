﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Controller : MonoBehaviour
{
    public GameObject pikminRedUI;
    public GameObject pikminBlueUI;
    public GameObject pikminYellowUI;
    
    public delegate void GeneratePikmin();
    public event GeneratePikmin GeneratePikminEvent;

    public delegate void UpdatePikminNum(int n);
    public event UpdatePikminNum UpdatePikminNumEvent;

    public delegate void UpdateFollowingPikminNum(int n);
    public event UpdateFollowingPikminNum UpdateFollowingPikminNumEvent;

    public delegate void UpdateColorPikminNum(int n);
    public event UpdateColorPikminNum UpdateColorPikminNumEvent;

    public int nPikmins;

    public GameObject gameCamera;
    public GameObject player;
    public GameObject pikmin;

    public LinkedList<GameObject> listRedPikmin = new LinkedList<GameObject>();
    public LinkedList<GameObject> listBluePikmin = new LinkedList<GameObject>();
    public LinkedList<GameObject> listYellowPikmin = new LinkedList<GameObject>();
    int actualPikminMap;

    public AudioSource sfxwhistle;
    public AudioSource bgmlevel1;
    public AudioSource sfxdeath1;
    public AudioSource sfxdeath2;
    public AudioSource sfxsendPikmin1;
    public AudioSource sfxsendPikmin2;
    public AudioSource sfxcallPikmin1;
    public AudioSource sfxcallPikmin2;
    public AudioSource sfxOlimarHurt;

    enum pikminType { RED, BLUE, YELLOW };
    pikminType actualColor;

    public Sprite pikminRedBubble;
    public Sprite pikminBlueBubble;
    public Sprite pikminYellowBubble;
    public GameObject actualPikminPhoto;

    public int redDrop;
    public int blueDrop;
    public int yellowDrop;

    public bool shipPart1, shipPart2, shipPart3;

    // Start is called before the first frame update
    void Start()
    {
        pikminRedUI.gameObject.SetActive(false);
        pikminBlueUI.gameObject.SetActive(false);
        pikminYellowUI.gameObject.SetActive(false);

        actualColor = pikminType.RED;

        bgmlevel1.Play();

        shipPart1 = false;
        shipPart2 = false;
        shipPart3 = false;

        pikminRedUI.GetComponent<PikminUI>().sumPikminControllerEvent += countGeneralPikmin;
        pikminBlueUI.GetComponent<PikminUI>().sumPikminControllerEvent += countGeneralPikmin;
        pikminYellowUI.GetComponent<PikminUI>().sumPikminControllerEvent += countGeneralPikmin;
    }

    
    // Update is called once per frame
    void Update()
    {
        //CANVI DE COLOR DE PIKMIN
        if(Input.GetKeyDown("x")) {
            if(actualColor == pikminType.RED)
            {
                actualColor = pikminType.BLUE;
                actualPikminPhoto.GetComponent<SpriteRenderer>().sprite = pikminBlueBubble;
                if (UpdateColorPikminNumEvent != null)
                {
                    UpdateColorPikminNumEvent.Invoke(listBluePikmin.Count);
                }
            }
            else if (actualColor == pikminType.BLUE)
            {
                actualColor = pikminType.YELLOW;
                actualPikminPhoto.GetComponent<SpriteRenderer>().sprite = pikminYellowBubble;
                if (UpdateColorPikminNumEvent != null)
                {
                    UpdateColorPikminNumEvent.Invoke(listYellowPikmin.Count);
                }
            }
            else if (actualColor == pikminType.YELLOW)
            {
                actualColor = pikminType.RED;
                actualPikminPhoto.GetComponent<SpriteRenderer>().sprite = pikminRedBubble;
                if (UpdateColorPikminNumEvent != null)
                {
                    UpdateColorPikminNumEvent.Invoke(listRedPikmin.Count);
                }
            }
        }

        //Instancia nous Pikmin en el Mapa quan es demanen per crear-se
        if(GeneratePikminEvent != null)
        {
            GeneratePikminEvent.Invoke();
        }

        gameCamera.transform.position = new Vector3(player.transform.position.x, player.transform.position.y, gameCamera.transform.position.z);

        if (Input.GetKeyDown("z"))
        {
            sfxwhistle.Play();
        }
    }

    //Retorna el color del tipus de Pikmin que hi ha seleccionats en aquest instant
    public int getActualColor()
    {
        if (actualColor == pikminType.RED)
            return 0;
        else if (actualColor == pikminType.BLUE)
            return 1;
        else if (actualColor == pikminType.YELLOW)
            return 2;
        else
            return 0;
    }

    //Envia a la UI el número de Pikmin que hi ha cada cop que es crea un de nou
    void countGeneralPikmin(int n)
    {
        actualPikminMap += n;
        if (UpdatePikminNumEvent != null)
        {
            UpdatePikminNumEvent.Invoke(actualPikminMap);
        }
    }

    //Envia a la UI el número de Pikmin que hi ha cada cop que es mor un
    public void countGeneralPikminRest()
    {
        actualPikminMap-=1;
        int n = Random.Range(0, 2);
        if (n == 0)
        {
            sfxdeath1.Play();
        }
        else if(n == 1)
        {
            sfxdeath2.Play();
        }
        if (UpdatePikminNumEvent != null)
        {
            UpdatePikminNumEvent.Invoke(actualPikminMap);
        }
    }

    //Envia a la UI el número de Pikmin que segueixen a Olimar cada cop que un Pikmin el segueix de nou
    public void countFollowingPikmin()
    {
        if (UpdateFollowingPikminNumEvent != null)
        {
            UpdateFollowingPikminNumEvent.Invoke(listRedPikmin.Count+listBluePikmin.Count+listYellowPikmin.Count);
        }
        if (actualColor == pikminType.RED)
        {
            if (UpdateColorPikminNumEvent != null)
            {
                UpdateColorPikminNumEvent.Invoke(listRedPikmin.Count);
            }
        }
        else if (actualColor == pikminType.BLUE)
        {
            if (UpdateColorPikminNumEvent != null)
            {
                UpdateColorPikminNumEvent.Invoke(listBluePikmin.Count);
            }
        }
        else if (actualColor == pikminType.YELLOW)
        {
            if (UpdateColorPikminNumEvent != null)
            {
                UpdateColorPikminNumEvent.Invoke(listYellowPikmin.Count);
            }
        }
    }

    //So al enviar els Pikmins
    public void sfxSendPikmin()
    {
        int n = Random.Range(0, 2);
        if (n == 0)
        {
            sfxsendPikmin1.Play();
        }
        else if (n == 1)
        {
            sfxsendPikmin2.Play();
        }
    }

    //So al cridar els Pikmins
    public void sfxCallPikmin()
    {
        int n = Random.Range(0, 2);
        if (n == 0)
        {
            sfxcallPikmin1.Play();
        }
        else if (n == 1)
        {
            sfxcallPikmin2.Play();
        }
    }

    //So quan disminueix la vida del Olimar
    public void sfxOlimarHurtPlay()
    {
        if (!sfxOlimarHurt.isPlaying)
        {
            sfxOlimarHurt.Play();
        }
    }
}
