﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class OlimarShip : MonoBehaviour
{
    GameObject controller;
    public GameObject timer;
    // Start is called before the first frame update
    void Start()
    {

        controller = GameObject.Find("GameController");
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            //Escena win si el jugador té els 3 coleccionables principals
           if (controller.GetComponent<Controller>().shipPart1 && controller.GetComponent<Controller>().shipPart2 && controller.GetComponent<Controller>().shipPart3) {
                SceneManager.LoadScene("WinScene");
                Dades.playTime = timer.GetComponent<Timer>().duration - timer.GetComponent<Timer>().timeRemaining;
            }
        }
    }
}
