﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Dades
{
    //Nom del jugador
    public static string playerName;

    //Temps que ha tardat
    public static int playTime;

    //Collecionables extra
    public static bool extra1;
    public static bool extra2;
    public static bool extra3;
    public static bool extra4;
    public static bool extra5;
}
