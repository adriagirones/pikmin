﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Animation2D
{
    public bool loop = true;
    public bool pingpong = false;
    public float speed = 1.0f;
    public Sprite[] frames;

    float current_frame = 0.0f;
    int last_frame = 0;
    int loops = 0;
    enum typepingpong
    {
        forward,
        backward
    }
    typepingpong direction = typepingpong.forward;

    public Animation2D(Sprite[] s)
    {
        frames = s;
        last_frame = s.Length;
    }

    //Torna el frame actual de l'animació
    public Sprite GetCurrentFrame()
    {
        switch (direction)
        {
            case typepingpong.forward:
                {
                    current_frame += speed;
                    if (current_frame >= last_frame)
                    {
                        //current_frame = (loop || pingpong) ? 0.0f : last_frame - 1;

                        if(loop || pingpong)
                        {
                            current_frame = 0.0f;
                        }
                        else
                        {
                            current_frame = last_frame - 1;
                        }

                        if(pingpong)
                        {
                            direction = typepingpong.backward;
                        }
                        else
                        {
                            direction = typepingpong.forward;
                        }
                        
                        //direction = pingpong ? typepingpong.backward : typepingpong.forward;
                        loops++;
                    }
                }
                break;
            case typepingpong.backward:
                {
                    current_frame -= speed;
                    if (current_frame <= 0.0f)
                    {
                        current_frame = 0.0f;
                        direction = typepingpong.forward;
                        loops++;
                    }
                }
                break;
        }
        return frames[(int)current_frame];
    }

    //Diu si l'animació ha acabat per primer cop
    public bool Finished()
    {
        return loops > 0;
    }

    //Reseteja l'animació al frame 1
    public void Reset()
    {
        current_frame = 0.0f;
    }





}
