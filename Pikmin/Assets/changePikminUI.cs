﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class changePikminUI : MonoBehaviour
{
    public GameObject Pikmin;
    public GameObject Controller;

    public GameObject actualColorPikminNum;
    public GameObject actualPikminNum;
    public GameObject globalPikminNum;

    // Start is called before the first frame update
    void Start()
    {
        Controller = GameObject.Find("GameController");
        Controller.GetComponent<Controller>().UpdatePikminNumEvent += ActualitzarUINumGlobal;
        Controller.GetComponent<Controller>().UpdateFollowingPikminNumEvent += ActualitzarUINumPikmin;
        Controller.GetComponent<Controller>().UpdateColorPikminNumEvent += ActualitzarUINumColor;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    //Indica quants Pikmin hi ha en el mapa vius
    void ActualitzarUINumGlobal(int n)
    {
        if(n<10)
            globalPikminNum.GetComponent<TMPro.TextMeshProUGUI>().text = "0" + n;
        else
            globalPikminNum.GetComponent<TMPro.TextMeshProUGUI>().text = "" + n;
    }

    //Indica quants Pikmin segueixen a Olimar
    void ActualitzarUINumPikmin(int n)
    {
        if (n < 10)
            actualPikminNum.GetComponent<TMPro.TextMeshProUGUI>().text = "0" + n;
        else
            actualPikminNum.GetComponent<TMPro.TextMeshProUGUI>().text = "" + n;
    }

    //Indica quants Pikmin segueixen a Olimar depenent del color seleccionat
    void ActualitzarUINumColor(int n)
    {
        if (n < 10)
            actualColorPikminNum.GetComponent<TMPro.TextMeshProUGUI>().text = "0" + n;
        else
            actualColorPikminNum.GetComponent<TMPro.TextMeshProUGUI>().text = "" + n;
    }
}
