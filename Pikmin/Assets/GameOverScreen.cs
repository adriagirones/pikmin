﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOverScreen : MonoBehaviour
{
    public GameObject background;
    public GameObject retryButton;
    public AudioSource bgmgameover;

    // Start is called before the first frame update
    // Posa la musica de game Over i activa el boto per reintentar.
    void Start()
    {
        Invoke("activeButton", 5f);
        retryButton.SetActive(false);
        bgmgameover.Play();
    }

    // Update is called once per frame
    void Update()
    {
        if(background.GetComponent<SpriteRenderer>().color.r >= 0)
        {
            //El fons es va fent negre
            background.GetComponent<SpriteRenderer>().color = new Color(background.GetComponent<SpriteRenderer>().color.r - 0.005f, background.GetComponent<SpriteRenderer>().color.g - 0.005f, background.GetComponent<SpriteRenderer>().color.b - 0.005f);
        }
    }

    //Posa com actiu el boto de reintentar.
    void activeButton()
    {
        retryButton.SetActive(true);
    }

    //Carrega la escena del menu principal.
    public void toTheMenu()
    {
        SceneManager.LoadScene("Intro");
    }


}
