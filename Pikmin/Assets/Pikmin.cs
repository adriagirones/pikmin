﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pikmin : MonoBehaviour
{
    public float vel;

    public Sprite[] red_frames_animationUp;
    public Sprite[] red_frames_animationDown;
    public Sprite[] red_frames_animationLeft;
    public Sprite[] red_frames_animationRight;
    Animation2D red_animationUp;
    Animation2D red_animationDown;
    Animation2D red_animationLeft;
    Animation2D red_animationRight;

    public Sprite[] blue_frames_animationUp;
    public Sprite[] blue_frames_animationDown;
    public Sprite[] blue_frames_animationLeft;
    public Sprite[] blue_frames_animationRight;
    Animation2D blue_animationUp;
    Animation2D blue_animationDown;
    Animation2D blue_animationLeft;
    Animation2D blue_animationRight;

    public Sprite[] yellow_frames_animationUp;
    public Sprite[] yellow_frames_animationDown;
    public Sprite[] yellow_frames_animationLeft;
    public Sprite[] yellow_frames_animationRight;
    Animation2D yellow_animationUp;
    Animation2D yellow_animationDown;
    Animation2D yellow_animationLeft;
    Animation2D yellow_animationRight;

    Animation2D currentAnimation;

    GameObject controller;

    public delegate void updatePikminUInum();
    public event updatePikminUInum updatePikminUInumEvent;

    enum pikminType { RED, BLUE, YELLOW };
    pikminType colorPikmin;

    // Start is called before the first frame update
    void Start()
    {
        controller = GameObject.Find("GameController");
        red_animationUp = new Animation2D(red_frames_animationUp);
        red_animationUp.speed = 0.5f;
        red_animationDown = new Animation2D(red_frames_animationDown);
        red_animationDown.speed = 0.5f;
        red_animationLeft = new Animation2D(red_frames_animationLeft);
        red_animationLeft.speed = 0.5f;
        red_animationRight = new Animation2D(red_frames_animationRight);
        red_animationRight.speed = 0.5f;

        blue_animationUp = new Animation2D(blue_frames_animationUp);
        blue_animationUp.speed = 0.5f;
        blue_animationDown = new Animation2D(blue_frames_animationDown);
        blue_animationDown.speed = 0.5f;
        blue_animationLeft = new Animation2D(blue_frames_animationLeft);
        blue_animationLeft.speed = 0.5f;
        blue_animationRight = new Animation2D(blue_frames_animationRight);
        blue_animationRight.speed = 0.5f;

        yellow_animationUp = new Animation2D(yellow_frames_animationUp);
        yellow_animationUp.speed = 0.5f;
        yellow_animationDown = new Animation2D(yellow_frames_animationDown);
        yellow_animationDown.speed = 0.5f;
        yellow_animationLeft = new Animation2D(yellow_frames_animationLeft);
        yellow_animationLeft.speed = 0.5f;
        yellow_animationRight = new Animation2D(yellow_frames_animationRight);
        yellow_animationRight.speed = 0.5f;

        if (colorPikmin == pikminType.RED)
            currentAnimation = red_animationDown;
        else if (colorPikmin == pikminType.BLUE)
            currentAnimation = blue_animationDown;
        else if (colorPikmin == pikminType.YELLOW)
            currentAnimation = yellow_animationDown;

    }


    // Update is called once per frame
    void Update()
    {

        if (gameObject.GetComponent<Pathfinding.AIDestinationSetterPikmin>().getAIDestination().x - this.GetComponent<Transform>().position.x > 0 && gameObject.GetComponent<Pathfinding.AIDestinationSetterPikmin>().getAIDestination().y - this.GetComponent<Transform>().position.y > 0)
        {
            if (colorPikmin == pikminType.RED)
                currentAnimation = red_animationUp;
            else if (colorPikmin == pikminType.BLUE)
                currentAnimation = blue_animationUp;
            else if (colorPikmin == pikminType.YELLOW)
                currentAnimation = yellow_animationUp;
        }
        else if (gameObject.GetComponent<Pathfinding.AIDestinationSetterPikmin>().getAIDestination().x - this.GetComponent<Transform>().position.x > 0 && gameObject.GetComponent<Pathfinding.AIDestinationSetterPikmin>().getAIDestination().y - this.GetComponent<Transform>().position.y < 0)
        {
            if (colorPikmin == pikminType.RED)
                currentAnimation = red_animationRight;
            else if (colorPikmin == pikminType.BLUE)
                currentAnimation = blue_animationRight;
            else if (colorPikmin == pikminType.YELLOW)
                currentAnimation = yellow_animationRight;
        }
        else if (gameObject.GetComponent<Pathfinding.AIDestinationSetterPikmin>().getAIDestination().x - this.GetComponent<Transform>().position.x < 0 && gameObject.GetComponent<Pathfinding.AIDestinationSetterPikmin>().getAIDestination().y - this.GetComponent<Transform>().position.y < 0)
        {
            if (colorPikmin == pikminType.RED)
                currentAnimation = red_animationDown;
            else if (colorPikmin == pikminType.BLUE)
                currentAnimation = blue_animationDown;
            else if (colorPikmin == pikminType.YELLOW)
                currentAnimation = yellow_animationDown;

        }
        else if (gameObject.GetComponent<Pathfinding.AIDestinationSetterPikmin>().getAIDestination().x - this.GetComponent<Transform>().position.x < 0 && gameObject.GetComponent<Pathfinding.AIDestinationSetterPikmin>().getAIDestination().y - this.GetComponent<Transform>().position.y > 0)
        {
            if (colorPikmin == pikminType.RED)
                currentAnimation = red_animationLeft;
            else if (colorPikmin == pikminType.BLUE)
                currentAnimation = blue_animationLeft;
            else if (colorPikmin == pikminType.YELLOW)
                currentAnimation = yellow_animationLeft;

        }
        //Pikmin està quiet
        if (System.Math.Round(gameObject.GetComponent<Pathfinding.AIDestinationSetterPikmin>().getAIDestination().x * 100f) / 100f == System.Math.Round(this.GetComponent<Transform>().position.x * 100f) / 100f && System.Math.Round(gameObject.GetComponent<Pathfinding.AIDestinationSetterPikmin>().getAIDestination().y * 100f) / 100f == System.Math.Round(this.GetComponent<Transform>().position.y * 100f) / 100f)
        {
            currentAnimation.Reset();
        }

        this.GetComponent<SpriteRenderer>().sprite = currentAnimation.GetCurrentFrame();

        if (updatePikminUInumEvent != null)
        {
            updatePikminUInumEvent.Invoke();
        }

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Pikmin"))
        {
            Physics2D.IgnoreCollision(collision.collider, this.GetComponent<BoxCollider2D>());  //IGNORA LES COLISIONS AMB ELS ALTRES PIKMIN
        }
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        if (colorPikmin != pikminType.BLUE && collider.CompareTag("ColliderWater"))
        {
            Death();
        }
        else if (colorPikmin != pikminType.YELLOW && collider.CompareTag("ColliderElectric"))
        {
            Death();
        }
        else if (colorPikmin != pikminType.RED && collider.CompareTag("ColliderFire"))
        {
            Death();
        }
    }

    public int getPikminType()
    {
        if (colorPikmin == pikminType.RED)
            return 0;
        else if (colorPikmin == pikminType.BLUE)
            return 1;
        else if (colorPikmin == pikminType.YELLOW)
            return 2;
        else
            return 0;
    }

    public void setPikminType(int c)
    {
        if(c == 0)
        {
            colorPikmin = pikminType.RED;
        }
        else if (c == 1)
        {
            colorPikmin = pikminType.BLUE;
        }
        else if (c == 2)
        {
            colorPikmin = pikminType.YELLOW;
        }
        else{
            colorPikmin = pikminType.RED;
        }
    }

    public void Death()
    {
        //Borra els pikmin de les llistes del controller
        if (colorPikmin == pikminType.RED)
        {
            if (controller.GetComponent<Controller>().listRedPikmin.Contains(this.gameObject))
            {
                controller.GetComponent<Controller>().listRedPikmin.Remove(this.gameObject);
            }
        }
        else if (colorPikmin == pikminType.YELLOW)
        {
            if (controller.GetComponent<Controller>().listYellowPikmin.Contains(this.gameObject))
            {
                controller.GetComponent<Controller>().listYellowPikmin.Remove(this.gameObject);
            }
        }
        else if (colorPikmin == pikminType.BLUE)
        {
            if (controller.GetComponent<Controller>().listBluePikmin.Contains(this.gameObject))
            {
                controller.GetComponent<Controller>().listBluePikmin.Remove(this.gameObject);
            }
        }
        controller.GetComponent<Controller>().countGeneralPikminRest();
        controller.GetComponent<Controller>().countFollowingPikmin();
        Destroy(this.gameObject);
    }


}
