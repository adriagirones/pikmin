﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Olimar : MonoBehaviour
{
    public int vel;

    public Sprite[] frames_animationUp;
    public Sprite[] frames_animationDown;
    public Sprite[] frames_animationLeft;
    public Sprite[] frames_animationRight;
    Animation2D currentAnimation;
    Animation2D animationUp;
    Animation2D animationDown;
    Animation2D animationLeft;
    Animation2D animationRight;

    private enum direction { LEFT, RIGHT, UP, DOWN };
    private direction olimarDirection;

    public delegate void UpdateTarget();
    public event UpdateTarget UpdateTargetEvent;
    public int hp;
    public GameObject controller;


    // Start is called before the first frame update
    void Start()
    {
        animationUp = new Animation2D(frames_animationUp);
        animationUp.speed = 0.3f;

        animationDown = new Animation2D(frames_animationDown);
        animationDown.speed = 0.3f;

        animationLeft = new Animation2D(frames_animationLeft);
        animationLeft.speed = 0.2f;

        animationRight = new Animation2D(frames_animationRight);
        animationRight.speed = 0.2f;

        currentAnimation = animationDown;
    }

    // Update is called once per frame
    void Update()
    {

        if(UpdateTargetEvent != null)
        {
            UpdateTargetEvent.Invoke();
        }

        if (Input.GetKey("w"))
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(this.GetComponent<Rigidbody2D>().velocity.x, vel);
            olimarDirection = direction.UP;
            currentAnimation = animationUp;
        }
        else if (Input.GetKey("s"))
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(this.GetComponent<Rigidbody2D>().velocity.x, -vel);
            olimarDirection = direction.DOWN;
            currentAnimation = animationDown;
        }
        else
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(this.GetComponent<Rigidbody2D>().velocity.x, 0);
            if (olimarDirection == direction.UP)
            {
                animationUp.Reset();
                currentAnimation = animationUp;
            }
            else if (olimarDirection == direction.DOWN)
            {
                animationDown.Reset();
                currentAnimation = animationDown;
            }
        }

        if (Input.GetKey("a"))
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(-vel, this.GetComponent<Rigidbody2D>().velocity.y);
            olimarDirection = direction.LEFT;
            currentAnimation = animationLeft;
        }
        else if (Input.GetKey("d"))
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(vel, this.GetComponent<Rigidbody2D>().velocity.y);
            olimarDirection = direction.RIGHT;
            currentAnimation = animationRight;
        }
        else
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, this.GetComponent<Rigidbody2D>().velocity.y);

            if (olimarDirection == direction.LEFT)
            {
                animationLeft.Reset();
                currentAnimation = animationLeft;
            }
            else if (olimarDirection == direction.RIGHT)
            {
                animationRight.Reset();
                currentAnimation = animationRight;
            }
        }

        //Si la vida es 0 s'acaba la partida i carrega l'escena del Game Over.
        if(hp <= 0)
        {
            SceneManager.LoadScene("GameOver");
        }
        this.GetComponent<SpriteRenderer>().sprite = currentAnimation.GetCurrentFrame();

    }

    //Retorna la direccio en la que esta en aquest moment l'olimar.
    public int getDirection()
    {
        switch (olimarDirection)
        {
            case direction.LEFT:
                return 0;
            case direction.RIGHT:
                return 1;
            case direction.UP:
                return 2;
            case direction.DOWN:
                return 3;
            default:
                return 0;
        }
       
    }

    //Resta vida a l'olimar si colisiona amb foc, aigua o electricitat.
    private void OnTriggerStay2D(Collider2D collider)
    {
        if (collider.gameObject.CompareTag("ColliderFire") || collider.gameObject.CompareTag("ColliderWater") || collider.gameObject.CompareTag("ColliderElectric"))
        {
            this.hp--;
            controller.GetComponent<Controller>().sfxOlimarHurtPlay();
        }

    }

    //Resta vida si colisiona amb l'enemic i activa el so.
    private void OnCollisionStay2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Enemy"))
        {
            this.hp--;
            controller.GetComponent<Controller>().sfxOlimarHurtPlay();
        }
    }

}