# Controls
Controls per teclat:

| Tecles | Acció |
| ------ | ------ |
|A/W/S/D         |`Moure el personatge`        |
|Z               |`Cridar a tots els Pikmin del  mapa`            |
|X               |`Canviar el color actual de Pikmin a enviar`|

Controls per pantalla:

| Control | Acció |
| ------ | ------ |
|Click a sobre un Pikmin         |`Crida el Pikmin seleccionat i comença a seguir al personatge`        |
|Doble click pel món             |`Envia un Pikmin al lloc seleccionat`            |

# Gameplay
L'Olimar s'ha estrellat en un planeta desconegut habitat per unes criatures extranyes. Ha de recuperar les tres parts de la nau que són escencials per arreglar la nau i poder conseguir tornar a la Terra. 

<img src="Pikmin/Assets/Resources/readmeimage1.png" width="500">

A més, s'han perdut 5 peces més que no són necessàries per poder engegar-la, però estaria bé recuperar-les! Potser les han robat algun tipus d'enemic...

<img src="Pikmin/Assets/Resources/readmeimage2.png" width="500">

Els Pikmin t'ajudaran a investigar el món. Destrueix plantes i enemics per aconseguir nous tipus de Pikmin que t'ajudin a recollir les peces que no estiguin en el teu abast.

<img src="Pikmin/Assets/Resources/readmeimage3.png" width="500">

Ves amb compte amb el temps! Quan acabi el dia apareixen els grans perills, és probable que Olimar no pugui sobreviure a tals adversitats.

<img src="Pikmin/Assets/Resources/readmeimage4.png" width="500">

Al final del nivell, apareixerà el temps que s'ha tardat en passar el nivell i els coleccionables que s'han aconseguit.

<img src="Pikmin/Assets/Resources/readmeimage5.PNG" width="500">

## Ampliacions

- Pathfinding
> Els Pikmin tenen pathfinding amb un target que va canviant.
- Enemics que es van spawnejant 
> Hi ha enemics que al cap d'uns segons tornen a aparèixer.
- Animacions, efectes de so, interfícies avançades, escenes extres, etc...

